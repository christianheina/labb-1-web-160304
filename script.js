$(document).ready(function () {
  var $calculation = $('#calculation');
  var $result = $('#result');
  var $inputNumber = $('#number');
  var $input = $('input');
  var numbers = [];
  const resultText = $result.text();
  const calculationText = $calculation.text();
  
  function readCookies() {
    var cookies = document.cookie;
    var visits = 0;
    if (cookies.indexOf('visits') === 0){
      var temp = cookies.split('=');
      visits = parseInt(temp[1]);
    }
    visits++;
    var cookieString = getNumberStringWithCorrectEnding(visits);
    $('body').append('<p>You are here for the ' + visits + cookieString + ' time!</p>');
    document.cookie = 'visits=' + visits + ';';
  }
  
  function getNumberStringWithCorrectEnding (number) {
    var textNumber = '';
    number = Math.abs(number);
    if(number === 0){
      return textNumber;
    } else if(number > 3){
      textNumber = 'th';
    } else if (number > 2){
      textNumber = 'rd';
    } else if (number > 1){
      textNumber = 'nd';
    } else {
      textNumber = 'st';
    }
    return textNumber;
  }
  
  function operation (operator) {
    var parseInput = parseFloat($input.val());
    if ($input.val() === '' || parseInput === 0 || isNaN(parseInput)) {
      return;
    }
    addNumber(operator + parseInput);
  }
  
  function sum () {
    if (numbers.length === 0){
      return;
    }
    clearCalculation();
    var sum = 0;
    for (var i = 0, len = numbers.length; i < len; i++){
      sum += numbers[i];
    }
    $result.append('<span class="fontChange">'+ sum +'</span>');
    numbers = [];
  }
  
  function addNumber(input) {
    if(numbers.length === 0){
      input = parseFloat(input);
    }
    printCalculation(input);
    numbers.push(parseFloat(input));
  }
  
  function printCalculation(print) {
    $calculation.append('<span class="fontChange">' + print + '</span>');
    $result.text(resultText);
    $inputNumber.val('');
  }
  
  function clearCalculation() {
    $calculation.text(calculationText);
  }
  
  $('*').keypress(function (event) {
    var keyEvent = event.which;
    if(!((keyEvent >= 48 && keyEvent <= 57) || keyEvent === 46)){
      event.preventDefault();
    }
    switch (keyEvent) {
      case 43:
        operation('+');
        break;
      case 45:
        operation('-');
        break;
      case 61:
        sum();
        break; 
      case 13:
        sum();
        break; 
    }
  });
  
  $('button').on('click', function () {
    var value = $(this).text();
    if(value === '='){
      sum();
    } else if (value === '+' || value === '-'){
      operation(value);
    }
  });
  
  $('form').on('click', function () {
    return false;
  });
  readCookies();
});
